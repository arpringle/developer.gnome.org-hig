Reference
=========

Design reference materials.

.. toctree::
   :hidden:

   reference/keyboard
   reference/palette
   reference/backgrounds

.. cssclass:: tiled-toc

*  .. image:: img/tiles/reference-keyboard.svg
      :target: reference/keyboard.html
      :class: only-light
   .. image:: /img/tiles/reference-keyboard-dark.svg
      :target: reference/keyboard.html
      :class: only-dark
      
   :doc:`Standard Keyboard Shortcuts </reference/keyboard>` 

*  .. image:: img/tiles/reference-palette.svg
      :target: reference/palette.html
      :class: only-light
   .. image:: /img/tiles/reference-palette-dark.svg
      :target: reference/palette.html
      :class: only-dark
      
   :doc:`Color Palette </reference/palette>` 

*  .. image:: img/tiles/reference-backgrounds.svg
      :target: reference/backgrounds.html
      :class: only-light
   .. image:: /img/tiles/reference-backgrounds-dark.svg
      :target: reference/backgrounds.html
      :class: only-dark
      
   :doc:`Backgrounds </reference/backgrounds>`
