Design Principles
=================

The HIG and the GNOME platform are reflective of the GNOME design tradition and philosophy, which has been informed by the project's collective experience creating user-facing software for over 20 years.

Designers and developers building for the GNOME platform are encouraged to familiarize themselves with our design philosophy, as this will enhance their ability to produce beautiful, effective, attractive, easy to use apps.

Design for People
-----------------

People are at the heart of GNOME design. Wherever possible, we seek to be as inclusive as possible. This means accommodating different physical abilities, cultures, and device form factors. Our software requires little specialist knowledge and technical ability.

The drive to create accommodating software threads its way through each of our guiding principles.

.. image:: img/design-for-people.svg

Make it Simple
--------------

The best apps do one thing and do it well. Often this requires having insight into the goals of your app, not just in functional terms but also how it will be used and fit into peoples' lives. The principle of simplicity applies to each view and element of your app, as well as the app as a whole.

* Resist the pull to try and make an app that suits all people in all situations. Focus on one situation, one type of experience.
* Don't overwhelm people with too many elements at once. Use progressive disclosure and navigation structures to provide a guided experience.
* Frequently used actions should be close at hand, with less important actions being further away.


Reduce User Effort
------------------

It is our job as software creators to reduce the amount of work and effort that people have to expend. This often means anticipating user needs, which requires having insight into the kind of situations and people your app is for.

* If something can be done automatically, do it automatically.
* Try to minimize the number of steps required to perform a task.
* Reduce the amount of information that people need to remember while using your app (tabs, recently used lists, and automatic suggestions are all effective techniques in this respect).
* Keep text short and to the point.


Be Considerate
--------------

Anticipating user needs goes beyond providing useful functionality. It also requires thinking about what people don't want from your app.

* Try to anticipate and prevent mistakes.
* Allow destructive actions to be undone instead of asking for confirmation.
* Respect people's time and attention. Don't interrupt or distract them unnecessarily.
